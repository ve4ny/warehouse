<?php

namespace App\Http\Controllers;

use App\Http\Resources\AllProductsResource;
use App\Models\Product;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with('stocks')->paginate(25);
        AllProductsResource::collection($products);
        return response()->json([
            'data' => $products->all(),
            'currentPage' => $products->currentPage(),
            'lastPage' => $products->lastPage()
        ]);
    }
}
