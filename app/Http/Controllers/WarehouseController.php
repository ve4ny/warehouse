<?php

namespace App\Http\Controllers;

use App\Http\Resources\AllWarehousesResource;
use App\Models\Warehouse;

class WarehouseController extends Controller
{
    public function index()
    {
        $warehouse = Warehouse::paginate(25);
        AllWarehousesResource::collection($warehouse);
        return response()->json([
            'data' => $warehouse->all(),
            'currentPage' => $warehouse->currentPage(),
            'lastPage' => $warehouse->lastPage()
        ]);
    }
}
