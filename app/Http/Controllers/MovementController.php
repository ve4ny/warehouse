<?php

namespace App\Http\Controllers;

use App\Actions\GetAllMovementsAction;
use App\Http\Resources\MovementResource;
use App\Models\Movement;
use Illuminate\Http\Request;

class MovementController extends Controller
{
    public function index(Request $request, GetAllMovementsAction $action)
    {
        $moves = $action->handle($request);
        MovementResource::collection($moves);
        if (count($moves->all()) === 0) {
            return response([
                'message' => 'Движений с такими параметрами нет'
            ], 404);
        }

        # Возвращаем найденные заказы постранично
        return response()->json([
            'data' => $moves->all(),
            'currentPage' => $moves->currentPage(),
            'lastPage' => $moves->lastPage()
        ]);
    }
}
