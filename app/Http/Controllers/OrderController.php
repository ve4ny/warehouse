<?php

namespace App\Http\Controllers;

use App\Actions\CheckAvailabilityAction;
use App\Actions\CreateOrderAction;
use App\Actions\GetAllOrdersAction;
use App\Actions\RestoreOrderAction;
use App\Actions\ReturnItemsToStockAction;
use App\Http\Resources\OrderResource;
use App\Services\OrderCreateService;
use App\Services\OrderUpdateService;
use Illuminate\Http\Request;
use App\Models\Order;
use DateTime;


class OrderController extends Controller
{
    public function index(
        Request            $request,
        GetAllOrdersAction $action
    )
    {
        # Ищем заказы по параметрам, возвращаем 404, если заказов нет
        $orders = $action->handle($request);
        OrderResource::collection($orders);
        if (count($orders->all()) === 0) {
            return response([
                'message' => 'Заказов с такими параметрами нет'
            ], 404);
        }

        # Возвращаем найденные заказы постранично
        return response()->json([
            'data' => $orders->all(),
            'currentPage' => $orders->currentPage(),
            'lastPage' => $orders->lastPage()
        ]);
    }


    public function show(string $id)
    {
        # Находим заказ по id
        $order = Order::findOrFail($id);

        # Возвращаем найденный заказ
        $order = new OrderResource($order);
        return response()->json([
            'data' => $order
        ]);
    }


    public function create(
        Request                 $request,
        CheckAvailabilityAction $checkAvailable,
        CreateOrderAction       $create,
        OrderCreateService      $service
    )
    {
        # Проверяем склады на наличие товара
        $warehouse = $checkAvailable->handle($request->items);
        if (!$warehouse) {
            return response([
                'message' => 'Указанного количества товаров нет ни на одном складе'
            ], 400);
        }

        # Создаем запись в таблице 'orders'
        $order = $create->handle($request->customer, $warehouse);

        # Создаем запись в таблице 'order_items', списываем товары со склада
        $isCreated = $service->create($request, $order);

        # В случае неуспеха, отменяем заказ
        if (!$isCreated) {
            $order->status = Order::STATUS_CANCELED;
        }

        # Возвращаем созданный заказ
        return response()->json([
            'data' => new OrderResource($order)
        ]);
    }


    public function update(
        string             $id,
        Request            $request,
        OrderUpdateService $service
    )
    {
        # Находим заказ и проверяем его статус
        $order = Order::findOrFail($id);
        if ($order->status != Order::STATUS_ACTIVE) {
            return response([
                'message' => 'Вы пытаетесь изменить не активный заказ'
            ], 400);
        }

        # Если в параметрах есть новое имя заказчика, меняем
        if ($request->customer) {
            $order->update([
                "customer" => $request->customer,
            ]);
        }

        # Если в параметрах есть новые товары, меняем
        if ($request->items) {
            $service->update($order, $request);
        }

        # Возвращаем обновленный заказ
        return response()->json([
            'data' => new OrderResource($order)
        ]);
    }


    public function complete(string $id)
    {
        $order = Order::find($id);
        $order->completed_at = new DateTime();
        $order->status = Order::STATUS_COMPLETED;
        $order->save();
        return response()->json([
            "data" => new OrderResource($order)
        ]);
    }


    public function cancel(string $id)
    {
        $order = Order::find($id);

        # Проверяем статус заказа. Возвращаем ошибку и сообщение, если заказ не активный
        $message = '';
        switch ($order->status) {
            case Order::STATUS_COMPLETED :
                $message = 'Ошибка. Заказ уже завершён';
                break;
            case Order::STATUS_CANCELED :
                $message = 'Ошибка. Заказ уже отменён';
                break;
        }

        if ($message) {
            return response()->json([
                'message' => $message
            ], 400);
        }

        # Меняем статус заказа
        $order->status = Order::STATUS_CANCELED;
        $order->save();

        # Возвращаем товары на склад
        $return = new ReturnItemsToStockAction;
        $return->handle($order, false);

        #Возвращаем обЪект заказа
        return response()->json([
            'data' => new OrderResource($order)
        ]);
    }


    public function restore(
        string             $id,
        RestoreOrderAction $action)
    {
        $restore = $action->handle($id);
        if (!$restore) {
            return response(null, 400);
        }
        $order = new OrderResource(Order::findOrFail($id));
        return response()->json([
            "data" => $order
        ]);
    }
}
