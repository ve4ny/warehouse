<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Трансформирует ресурс в массив
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'customer' => $this->customer,
            'warehouse' => $this->warehouses->name,
            'created_at' => $this->created_at,
            'status' => $this->status,
            'items' => ProductResource::collection($this->items->where('pivot.count', '!=', 0))
        ];
    }
}
