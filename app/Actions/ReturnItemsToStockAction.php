<?php

namespace App\Actions;

use App\Models\Order;
use App\Models\Warehouse;


class ReturnItemsToStockAction
{
    /**
     * Создание записи в таблице 'order_items', списание со склада, фиксация движения товара.
     *
     * @param Order       $order        Модель заказа
     * @param bool        $reset        Булево значение - нужно сбрасывать количество товаров в заазе или нет
     */
    public function handle(Order $order, bool $reset)
    {
        $oldItems = $order->items()->get();
        $move = new CreateItemMovementAction;
        foreach ($oldItems as $oi) {
            # Возвращаем товары в 'stock'
            $stockTable = Warehouse::with('products')
                ->find($order->warehouse_id)
                ->products
                ->find($oi['id'])
                ->pivot;
            $stockCount = $stockTable->stock;
            $stockTable->update(['stock' => $stockCount + $oi->pivot->count]);


            # Фиксируем движение товара в таблице 'movements'
            $item = [
                'id' => $oi->id,
                'count' =>$oi->pivot->count
            ];
            $move->handle($item, $order->warehouse_id, false);

            # Если получили в параметрах булево значение $reset=true обнуляем количество товара в заказе
            if ($reset)
                $oi->pivot->update(['count' => 0]);
        }
    }
}
