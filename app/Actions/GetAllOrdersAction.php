<?php

namespace App\Actions;

use App\Models\Order;
use Illuminate\Http\Request;

class GetAllOrdersAction
{
    /**
     * Проверка доступности товаров на всех складах.
     *
     * @param Request     $request      Реквест
     */

    public function handle(Request $request)
    {
        $query = Order::query();

        # Если в запросе есть поле 'customer' - фильтроем по стобцу 'customer'

        if($request->string('customer') != '') {
            $query->where('customer', $request->string('customer'));
        }

        # Если в запросе есть поле 'warehouse_id' - фильтроем по стобцу 'warehouse_id'

        if($request->string('warehouse_id') != '') {
            $query->where('warehouse_id', $request->string('warehouse_id'));
        }

        # Если в запросе есть поле 'status' - фильтроем по стобцу 'status'

        if($request->string('status') != '') {
            $query->where('status', $request->string('status'));
        }

        # Если в запросе есть поле date - фильтруем по столбцу 'created_at'

        if($request->string('date') != '') {
            $query->whereDate('created_at', $request->string('date'));
        }

        # Настраиваем пагинацю. По умолчанию 10 заказов на странице.
        $pagination = $request->integer('paginate') ? $request->integer('paginate') : 10;
        return $query->with('items')->paginate($pagination);
    }
}
