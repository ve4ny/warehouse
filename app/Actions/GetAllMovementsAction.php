<?php

namespace App\Actions;

use App\Models\Movement;
use Illuminate\Http\Request;

class GetAllMovementsAction
{
    /**
     * Проверка доступности товаров на всех складах.
     *
     * @param Request     $request      Реквест
     */

    public function handle(Request $request)
    {
        $query = Movement::query();

        # Если в запросе есть поле 'product_id' - фильтроем по стобцу 'product_id'

        if($request->product_id != '') {
            $query->where('product_id', $request->product_id);
        }

        # Если в запросе есть поле 'warehouse_id' - фильтроем по стобцу 'warehouse_id'

        if($request->warehouse_id != '') {
            $query->where('warehouse_id', $request->warehouse_id);
        }

        # Если в запросе есть поле date - фильтруем по столбцу 'created_at'

        if($request->string('date') != '') {
            $query->whereDate('created_at', $request->string('date'));
        }

        # Настраиваем пагинацю. По умолчанию 10 заказов на странице.
        $pagination = $request->integer('paginate') ? $request->integer('paginate') : 10;
        return $query->paginate($pagination);
    }
}
