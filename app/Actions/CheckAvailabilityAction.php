<?php

namespace App\Actions;

use App\Models\Warehouse;
use Illuminate\Database\Eloquent\Model;


class CheckAvailabilityAction
{
    /**
     * Проверка доступности товаров на всех складах.
     *
     * @param array     $items      Массив товаров из заказа
     */

    public function handle(array $items): Model
    {
        # Билдим eloquent запрос Склада, на котором есть доступные товары
        $query = Warehouse::query();
        $query->with('products');
        foreach($items as $item) {
            $query->whereHas('products', function ($builder) use($item) {
                $builder->where( 'id', $item['id'])->where('stock', '>', $item['count']);
            });
        }
        return $query->first();
    }
}
