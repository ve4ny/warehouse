<?php

namespace App\Actions;


use App\Models\Order;
use App\Models\Warehouse;

class WriteOffItemsFromStockAction
{
    public function handle(Order $order)
    {
        $move = new CreateItemMovementAction;
        $items = $order->items()->whereNot('count', 0)->get();
        foreach($items as $oi) {
            $stockTable = Warehouse::with('products')
                ->find($order->warehouse_id)
                ->products
                ->find($oi['id'])
                ->pivot;
            $stockCount = $stockTable->stock;

            # Списываем товар со склада
            $stockTable->update(['stock' => $stockCount - $oi->pivot->count]);

            #  Фиксируем списание в таблице 'movements'
            $item = [
                    'id' => $oi->id,
                    'count' => $oi->pivot->count
                ];
            $move->handle($item, $stockTable->warehouse_id, true);
        }
    }
}
