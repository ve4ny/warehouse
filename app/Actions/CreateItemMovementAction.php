<?php

namespace App\Actions;

use App\Models\Movement;
use DateTime;

class CreateItemMovementAction
{
    public function handle(array $item, int $warehouse_id, bool $writeOff)
    {
        Movement::create([
            'product_id' => $item['id'],
            'warehouse_id' => $warehouse_id,
            'action' => $writeOff ? - $item['count'] : + $item['count'],
            'created_at' => new DateTime()
        ]);
    }
}
