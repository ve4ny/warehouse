<?php

namespace App\Actions;

use App\Models\Order;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class CreateOrderAction
{
    /**
     * Создание строки в таблице 'orders'.
     *
     * @param string     $customer      Строка с именем заказчика
     * @param object     $warehouse     Объект склада
     */

    public function handle(String $customer, object $warehouse)
    {
        return Order::create([
            'customer' => $customer,
            'status' => Order::STATUS_ACTIVE,
            'created_at' => new DateTime(),
            'warehouse_id' => $warehouse->id
        ]);
    }
}
