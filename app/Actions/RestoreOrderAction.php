<?php

namespace App\Actions;

use App\Models\Order;

class RestoreOrderAction
{
    public function handle(string $id) : bool
    {
        # Находим заказ и проверяем его статус.
        $order = Order::findOrFail($id);
        if($order->status != Order::STATUS_CANCELED) {
            return false;
        }
        else {
            $items = $order->items()->whereNot('count', 0)->get()->map(function($i) {
                return [
                    "id" => $i->id,
                    "count" => $i->pivot->count
                ];
            })->toArray();

            # Проверяем доступность товаров на складах
            $checkAvailable = new CheckAvailabilityAction;
            $warehouse = $checkAvailable->handle($items);
            if($warehouse->count() > 0) {

                # Списываем товары со склада
                $writeOff = new WriteOffItemsFromStockAction;
                $writeOff->handle($order);

                # Меняем статус заказа на 'active'
                $order->warehouse_id = $warehouse->id;
                $order->status = Order::STATUS_ACTIVE;
                $order->save();
                return true;
            }
            else return false;
        }
    }
}
