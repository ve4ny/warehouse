<?php

namespace App\Services;

use App\Actions\CreateItemMovementAction;
use App\Models\Order;
use App\Models\Warehouse;
use Illuminate\Http\Request;

class OrderCreateService
{
    /**
     * Создание записи в таблице 'order_items', списание со склада.
     *
     * @param Request     $request      Реквест
     * @param Order       $order        Модель заказа
     */

    public function create(Request $request, Order $order): bool
    {
        $res = [];
        foreach ($request->items as $item) {
            # Создаем запись в pivot таблице Order_items
            $order->items()->attach($item['id'], ['count' => $item['count']]);

            # Списываем товары со склада
            $stock = Warehouse::with('products')
                ->find($order->warehouse_id)
                ->products
                ->find($item['id'])
                ->pivot;

            $stock->stock -= $item['count'];

            # Создаем запись движения товара
            $move = new CreateItemMovementAction;
            $move->handle($item, $stock->warehouse_id, true);

            $res[] = $stock->save();
        }
        return !in_array(false, $res, true);
    }
}
