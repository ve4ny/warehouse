<?php

namespace App\Services;

use App\Actions\CheckAvailabilityAction;
use App\Actions\CreateItemMovementAction;
use App\Actions\ReturnItemsToStockAction;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderUpdateService
{
    /**
     * Обновление заказа.
     *
     * @param Request     $request      Реквест
     * @param Order       $order        Модель заказа
     */

    public function update(
        Order                   $order,
        Request                 $request,
    ) : bool
    {

        # Возвращаем товары на склад, обнуляем старые позиции в заказе
        $return = new ReturnItemsToStockAction;
        $return->handle($order, true);

        # Проверяем доступность товаров на складах
        $action = new CheckAvailabilityAction;

        # Меняем warehouse_id в заказе на id склада, на котором есть необходимые товары
        $warehouse = $action->handle($request->items);
        $order->warehouse_id = $warehouse->id;
        $order->save();

        # Пересоздаем новые позиции в заказе
        $service = new OrderCreateService;
        return $service->create($request, $order);
    }
}
