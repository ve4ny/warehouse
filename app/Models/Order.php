<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Order extends Model
{
    use HasFactory;

    public $timestamps = false;

    const STATUS_ACTIVE = 'active';
    const STATUS_COMPLETED = 'completed';
    const STATUS_CANCELED = 'canceled';

    protected $fillable = [
        'customer',
        'created_at',
        'completed_at',
        'status',
        'warehouse_id'
    ];

    public function items(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'order_items')->withPivot('count');
    }

    public function warehouses() : BelongsTo
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }
}
