<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function orders() : BelongsToMany
    {
        return $this->belongsToMany(Order::class, 'order_items')->withPivot('count');
    }

    public function stocks() : BelongsToMany
    {
        return $this->belongsToMany(Warehouse::class, 'stocks')->withPivot('stock');
    }

}
