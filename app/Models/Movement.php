<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Movement extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable =[
        'product_id',
        'warehouse_id',
        'action',
        'created_at'
    ];

    public function products(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function warehouses(): BelongsTo
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }


}
