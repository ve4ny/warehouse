<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Product;
use App\Models\Stock;
use App\Models\Warehouse;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Product::factory(200)->create();
        Warehouse::factory(10)->create();

        $wCount = Warehouse::count();

        Warehouse::all()->each(function ($w) use ($wCount) {
            $w->products()->attach(
                Product::all()->random(rand(1, $wCount))->pluck('id')->toArray()
            );
        });
    }
}
