<?php

use App\Http\Controllers\MovementController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\WarehouseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::prefix('v1.0')->group(function() {
    Route::get('/warehouses', [WarehouseController::class, 'index']);

    Route::get('/products', [ProductController::class, 'index']);

    Route::get('/movements', [MovementController::class, 'index']);

    Route::get('/orders', [OrderController::class, 'index']);

    Route::get('/order/{id}', [OrderController::class, 'show']);

    Route::post('/order/create', [OrderController::class, 'create']);

    Route::put('/order/{id}/update', [OrderController::class, 'update']);

    Route::post('/order/{id}/complete', [OrderController::class, 'complete']);

    Route::post('/order/{id}/cancel', [OrderController::class, 'cancel']);

    Route::post('/order/{id}/restore', [OrderController::class, 'restore']);
});

